// Plugins
import React from 'react';
import ReactDOM from 'react-dom';

// Components
import Layout from './components/Layout';

// Render everything to the container
ReactDOM.render(<Layout />, document.getElementById('app'));
