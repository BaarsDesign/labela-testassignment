/*
	This file contains the main layout.

	TODO: Move all data-related stuff to another file.
*/

// Plugins
import React from "react";
import Request from 'superagent';
import Uuid from 'uuid';

// Components
import Nav from "./Nav";
import Category from "./Category";


export default class Layout extends React.Component {
	constructor() {
		super();
		this.state = {
			categories: [],
			active: "people",
			content: ""
		}
	}

	componentWillMount() {
		this.getNavItems();
	}

	getNavItems() {

		// Get menu items
		Request.get('https://swapi.co/api/').end((error, result) => {

			// Save them to data
			const data = result.body;
			let sorted = [];

			// If there are any errors, exit
			if(error) {
				console.log(error);
				exit;
			}

			// Change the structure of the data so we can use it
			for (let category in data) {
				if (data.hasOwnProperty(category)) {
					// Push it to the sorted array in a new format
					sorted.push({
						key: Uuid.v4(),
						name: category,
						url: data[category]
					});
				}
			}

			// Save it to the state
			this.setState({categories: sorted});
		});
	}

	// This gets fired when someone clicks a nav item.
	handleNav(active) {
		this.setState({ active });
	}

	render() {
		// Make sure we render the latest version of the content

		return (
			<div className="container">
				<Nav items={this.state.categories} handleNav={this.handleNav.bind(this)} />

				<Category category={this.state.active} />
			</div>

		);
	}
}
