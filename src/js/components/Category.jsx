// Plugins
import React from "react";
import Request from 'superagent';

// Components
import Article from './Article';


export default class Category extends React.Component {

	constructor() {
		super();
		this.state = {
			category: '',
			data: []
		}
	}

	componentWillUpdate() {
		if(this.props.category !== this.state.category) {
			this.setState({category: this.props.category}, () => {
				this.getData();
			});
		}

	}

	getData() {
		// Get the data
		Request.get(`https://swapi.co/api/${this.state.category}`).end((error, result) => {

			// Save them to data
			const data = result.body.results;

			// If there are any errors, exit
			if(error) {
				console.log(error);
				exit;
			}

			// Save it to the state
			this.setState({ data: data });
			console.log("Got new data:");
			console.log(data);
		});
	}

	render() {
		return (
			<div className="App--content">
				<div className="page-header">
					<h1>{this.props.category}</h1>
				</div>

				<Article data={this.state.data} />

			</div>

		);
	}
}
