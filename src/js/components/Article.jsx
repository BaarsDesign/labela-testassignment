// Plugins
import React from "react";
import Uuid from 'uuid';

// Styles
// import Styles from "../../css/components/Article.css"

export default class Article extends React.Component {
	constructor() {
		super();
		this.state = {
			data: [],
			articleItems: ""
		}
	}

	componentWillUpdate() {
		if(JSON.stringify(this.props.data) !== JSON.stringify(this.state.data)) {
			this.setState({ data: this.props.data}, () => {
				this.handleData();
			});
		}
	}


	handleData() {
		const { data } = this.state;

		// Generate the HTML for every menu-item
		const articleItems = data.map((item, index) => {


			// Filters out the stuf we don't want
			const filters = ['name', 'title', 'films', 'url', 'pilots', 'edited', 'created', 'vehicles', 'starships', 'species', 'homeworld', 'residents', 'characters', 'planets', 'people']

			// Because an object doesn't have the map function, map Object.keys()
			let details = Object.keys(item).map((key, index) => {
				// Filter out the unwanted stuff
 				if(filters.indexOf(key) == -1) {
					// Make the name pretty
					let name = key.replace(/_/g, " ").toUpperCase();

					return(
						<tr key={Uuid.v4()}>
							<td>{name}</td>
							<td>{item[key]}</td>
						</tr>
					);
				}
			});

			// Sometimes there is a title in stead of a name.
			// Kind of a hacky fix, but it works
			if(item.title) item.name = item.title;

			return (
				<div className="col" key={Uuid.v4()}>
					<table class="table table-striped table-hover table-bordered">
						<thead className="thead-dark">
							<tr>
								<th colSpan="2">{item.name}</th>
							</tr>
						</thead>
						<tbody>
							{details}
						</tbody>
					</table>
				</div>
			);
		});
		this.setState({ articleItems });
	}


	render() {
		return (
			<div>
				<div className="row">
					<div className="col">
						<p>
							A long time ago, in a galaxy far, far away...
						</p>
						<p>
							It is a period of civil war. Rebel spaceships, striking from a hidden base, have won their first victory against the evil Galactic Empire. During the battle, Rebel spies managed to steal secret plans to the Empire's ultimate weapon, the Death Star, an armored space station with enough power to destroy an entire planet. Pursued by the Empire's sinister agents, Princess Leia races home aboard her starship, custodian of the stolen plans that can save her people and restore freedom to the galaxy....
						</p>
					</div>
				</div>
				<div className="row">
					{this.state.articleItems}
				</div>
			</div>
		);
	}
}
