/*
	This file contains the navigation bar

	TODO: Fix direct deeplinking
*/

// Plugins
import React from "react";

export default class Nav extends React.Component {

	constructor() {
		super();
		this.state = {
			active: "people"
		}
	}

	handleNav(e) {
		// Get the clicked category
		let { category } = e.target.dataset;

		// Save it to the state
		this.setState({active: category});
		
		// Send it back to the Layout component
		this.props.handleNav(category);
	}

	render() {
		/*
			TODO: It's not the right place to do this:
		*/
		// Generate the HTML for every menu-item
		const navItems = this.props.items.map((items) => {

			// Check if this menu-item is active; if so, add the 'active' class
			const activeClass = (items.name === this.state.active) ? 'active' : '';

			return (
				<li key={items.key} className={`nav-item ${activeClass} nav-item--${items.name}`}>
					<a onClick={this.handleNav.bind(this)} href={`#/${items.name}`} data-category={items.name} className="nav-link">{items.name}</a>
				</li>
			);
		});

		return (
			<nav className="navbar navbar-expand-lg fixed-top navbar-dark bg-primary">
				<div className="container">
					<a className="navbar-brand" href="#">StaarWars</a>
					<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
						<span className="navbar-toggler-icon"></span>
			  		</button>

				  	<div className="collapse navbar-collapse" id="navbarColor01">
						<ul className="nav navbar-nav ml-auto">

							{ navItems }

						</ul>
					</div>
				</div>
			</nav>
		);
	}
}
